<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('auth_m');
    }

    public function index()
    {
        $data['dashboard'] = true;
        $data['content'] = 'dashboard';
        $this->load->view('index', $data);
    }

    public function profil_saya()
    {
        $data['profil_saya'] = true;
        $data['profil'] = user(__session('iduser'));
        $data['content'] = 'profil';
        $this->load->view('index', $data);
    }

    public function profil_perusahaan()
    {
        $data['profil_perusahaan'] = true;
        $data['perusahaan'] = user(__session('iduser'));
        $data['content'] = 'perusahaan';
        $this->load->view('index', $data);
    }

    public function update_profil()
    {
        $data = [
            'email' => $this->input->post('email', true),
            'nik' => $this->input->post('nik', true),
            'nama_lengkap' => $this->input->post('nama_lengkap', true),
            'tempat_lahir' => $this->input->post('tempat_lahir', true),
            'tanggal_lahir' => $this->input->post('tanggal_lahir', true),
            'jenis_kelamin' => $this->input->post('jenis_kelamin', true),
            'agama' => $this->input->post('agama', true),
            'alamat' => $this->input->post('alamat', true),
            'nama_perusahaan' => $this->input->post('nama_perusahaan', true),
            'sektor_usaha' => $this->input->post('sektor_usaha', true),
            'jabatan_pimpinan' => $this->input->post('jabatan_pimpinan', true)
        ];
        $this->auth_m->updateProfil($data, $this->input->post('idprofil', true));
        $password = $this->input->post('password', true);
        if ($password != "") {
            $data = [
                'password' => password_hash($password, PASSWORD_DEFAULT)
            ];
            $this->auth_m->updatePass($data, $this->input->post('iduser', true));
        }
        $this->session->set_flashdata('success', 'Anda telah berhasil mengubah data profil');
        redirect('dashboard/profil_saya');
    }

    public function update_profil_perusahaan()
    {
        $data = [
            'nama_perusahaan' => $this->input->post('nama_perusahaan', true),
            'nama_sektor' => $this->input->post('nama_sektor', true),
            'alamat_perusahaan' => $this->input->post('alamat_perusahaan', true),
            'nama_pimpinan' => $this->input->post('nama_pimpinan', true),
            'jabatan_pimpinan' => $this->input->post('jabatan_pimpinan', true)
        ];
        $this->auth_m->updatePerusahaanProfil($data, $this->input->post('idperusahaan_profile', true));

        $this->session->set_flashdata('success', 'Anda telah berhasil mengupdate data profil perusahaan');
        redirect('dashboard/profil_perusahaan');
    }
}

/* End of file Dashboard.php */