<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Profil Saya</h1>
</div>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-body">
        <form action="<?= base_url('dashboard/update_profil_perusahaan'); ?>" method="post">
            <div class="row">

                <div class="col-md-8">
                    <div class="form-group">
                        <label for="nama_perusahaan">Nama Perusahaan</label>
                        <input type="text" class="form-control" name="nama_perusahaan" id="nama_perusahaan" value="<?= $data['nama_perusahaan']; ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="nama_sektor">Nama Sektor Perusahaan</label>
                        <input type="text" class="form-control" name="nama_sektor" id="nama_sektor" value="<?= $data['nama_sektor']; ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="nama_pimpinan">Nama Pimpinan</label>
                        <input type="text" class="form-control" name="nama_pimpinan" id="nama_pimpinan" value="<?= $data['nama_pimpinan']; ?>">
                    </div>
                </div>

                <hr>
                <div class="p-3 float-right">
                    <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Simpan
                        Perubahan</button>
                </div>
            </div>
        </form>
    </div>
</div>