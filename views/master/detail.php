<!-- DataTales Example -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Detail User <?= $perusahaan['nik']; ?></h1>
    </div>
    <div class="card shadow mb-4">
        <div class="p-3">
            <a href="<?= base_url('master/pengguna'); ?>" class="btn btn-secondary btn-icon-split btn-sm float-left">
                <span class="icon text-white-50">
                    <i class="fas fa-chevron-left"></i>
                </span>
                <span class="text">Kembali</span>
            </a>
        </div>
        <div class="card-body">
            <table class="table">
                <tr>
                    <td width="300">E-Mail</td>
                    <td width="5">:</td>
                    <td><?= $perusahaan['email']; ?></td>
                </tr>
                <tr>
                    <td width="300">Nama</td>
                    <td width="5">:</td>
                    <td><?= $perusahaan['nama_lengkap']; ?></td>
                </tr>
                <tr>
                    <td width="300">Nama Perusahaan</td>
                    <td width="5">:</td>
                    <td><?= $perusahaan['nama_perusahaan']; ?></td>
                </tr>
                <tr>
                    <td>Sektor Usaha</td>
                    <td>:</td>
                    <td><?= $perusahaan['sektor_usaha']; ?></td>
                </tr>
                <tr>
                    <td>Jabatan Pimpinan</td>
                    <td>:</td>
                    <td><?= $perusahaan['jabatan_pimpinan']; ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>