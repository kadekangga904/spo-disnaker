<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>

    <style>
        table,
        td,
        th {
            border: 1px solid #333;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        td,
        th {
            padding: 2px;
        }

        th {
            background-color: #ccc;
        }
    </style>
</head>

<body>
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Pengajuan</h1>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="50">NO</th>
                            <th>E-MAIL</th>
                            <th>USERNAME</th>
                            <th>NAMA PERUSAHAN</th>
                            <th>NAMA LENGKAP</th>
                            <th>NAMA SURAT</th>
                            <th>STATUS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $n = 1;
                        foreach ($pengajuan_all as $row) : ?>
                            <tr>
                                <td><?= $n++; ?></td>
                                <td><?= $row['email']; ?></td>
                                <td><?= $row['username']; ?></td>
                                <td><?= $row['nama_perusahaan']; ?></td>
                                <td><?= $row['nama_lengkap']; ?></td>
                                <td><?= $row['nama_surat']; ?></td>
                                <td>
                                    <?php if ($row['status'] == 'Buat') : ?>
                                        <span class="badge badge-dark"><?= $row['status']; ?></span>
                                    <?php elseif ($row['status'] == 'Pengajuan') : ?>
                                        <span class="badge badge-info"><?= $row['status']; ?></span>
                                    <?php elseif ($row['status'] == 'Proses') : ?>
                                        <span class="badge badge-primary"><?= $row['status']; ?></span>
                                    <?php elseif ($row['status'] == 'Ditolak') : ?>
                                        <span class="badge badge-danger"><?= $row['status']; ?></span>
                                    <?php else : ?>
                                        <span class="badge badge-success"><?= $row['status']; ?></span>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</body>

</html>